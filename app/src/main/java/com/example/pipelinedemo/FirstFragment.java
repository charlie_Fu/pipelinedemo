package com.example.pipelinedemo;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import com.microsoft.appcenter.analytics.Analytics;
import com.microsoft.appcenter.crashes.Crashes;

import java.security.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

public class FirstFragment extends Fragment {

    private static final String TAG = FirstFragment.class.getSimpleName();

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        Toast.makeText(getContext(), "hello world 2021 1215", Toast.LENGTH_SHORT).show();
        // Inflate the layout for this fragment
        SimpleDateFormat dateFormatForEls = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
        Date date = new Date(System.currentTimeMillis());
        String dateStr = dateFormatForEls.format(date);
        System.out.println("Current Time Stamp: " + dateStr);
//        Analytics.trackEvent(String.format("entry FirstFragment date : %s", dateStr));
        Analytics.trackEvent(String.format("entry FirstFragment"));


        try {
            int divByZero = 42 / 0;
        } catch (Exception e) {
            Log.d(TAG, String.format("onCreateView: %s", e.toString()));
            Crashes.trackError(e);
        }

        return inflater.inflate(R.layout.fragment_first, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        view.findViewById(R.id.button_first).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(FirstFragment.this)
                        .navigate(R.id.action_FirstFragment_to_SecondFragment);
            }
        });
    }
}