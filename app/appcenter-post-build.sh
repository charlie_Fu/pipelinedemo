#!/usr/bin/env bash

echo '=============================================================================='
echo 'task run post-build'
echo 'Description run appcenter-post-build.sh'
echo 'author : Morris'
echo '=============================================================================='

pwd
ls
echo `find . -name "*.apk"`
echo limit day -7
echo `find . -mtime -7 -name "*.apk"`
echo limit mins -60
echo `find . -mmin -60 -name "*.apk"`

version_name=$(find . -mmin -60 -name "*.apk")
version_name=${version_name:28}
echo $version_name

echo $APPCENTER_BRANCH
if [ "$APPCENTER_BRANCH" == "master" ]; then
   echo "release branch $APPCENTER_BRANCH and upload apk to ftp"
   curl "ftp://$FTP_HOST:$FTP_PORT/$FTP_PRODUCTION_PATH" -u "$FTP_USERNAME:$FTP_PASSWORD" -T `find . -name "*.apk"`

   echo "tag and push versionCName to bitbucket"
   git tag -a $version_name -m ''
   git push https://$BITBUCKET_USERNAME:$BITBUCKET_PASSWORD@bitbucket.org/charlie_Fu/aiellospot.git --tags
elif [ "$APPCENTER_BRANCH" == "beta" ]; then
   echo "upload beta"
   curl ftp://cloud.aiello.ai:2200/push/test/Ultron/ -u "strong_aiello_e4gGJ9:3btjhRTWPNPndQsx" -T `find . -name "*.apk"`
fi
echo 'finish Aiello task : post-build'
#curl ftp://cloud.aiello.ai:2200/ -u "strong_aiello_e4gGJ9:3btjhRTWPNPndQsx" -T `find . -name "*.apk"`
